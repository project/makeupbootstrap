<?php
/**
 * @file
 * makeupsemantic_bootstrap.semantic_fields.inc
 */

/**
 * Implements hook_default_semantic_fields_preset().
 */
function makeupsemantic_bootstrap_default_semantic_fields_preset() {
  $export = array();

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'bcarousel_item';
  $preset->admin_title = 'Bootstrap Carousel Item';
  $preset->description = '';
  $preset->data = array(
    'field_element' => '',
    'field_class' => '',
    'field_prefix' => '',
    'field_suffix' => '',
    'label_element_above' => '',
    'label_class_above' => '',
    'label_suffix_above' => '',
    'label_element_inline' => '',
    'label_class_inline' => '',
    'label_suffix_inline' => ':',
    'multiple_items_element' => 'div',
    'multiple_items_class' => 'carousel-inner',
    'multiple_item_element' => 'div',
    'multiple_item_class' => 'item',
    'multiple_item_separator' => '',
    'use_singlevalue_settings' => 0,
    'single_item_element' => 'div',
    'single_item_class' => '',
    'last_every_nth' => '0',
    'first_class' => '',
    'last_class' => '',
    'striping_classes' => '',
  );
  $export['bcarousel_item'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'pull_left';
  $preset->admin_title = 'Pull-left';
  $preset->description = '';
  $preset->data = array(
    'field_element' => '',
    'field_class' => '',
    'field_prefix' => '',
    'field_suffix' => '',
    'label_element_above' => 'h3',
    'label_class_above' => '',
    'label_suffix_above' => '',
    'label_element_inline' => 'span',
    'label_class_inline' => '',
    'label_suffix_inline' => ':',
    'multiple_items_element' => '',
    'multiple_items_class' => '',
    'multiple_item_element' => 'div',
    'multiple_item_class' => 'pull-left',
    'multiple_item_separator' => '',
    'use_singlevalue_settings' => 1,
    'single_item_element' => '',
    'single_item_class' => '',
    'last_every_nth' => '0',
    'first_class' => 'first',
    'last_class' => 'last',
    'striping_classes' => 'odd even',
  );
  $export['pull_left'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'pull_right';
  $preset->admin_title = 'Pull-right';
  $preset->description = '';
  $preset->data = array(
    'field_element' => '',
    'field_class' => '',
    'field_prefix' => '',
    'field_suffix' => '',
    'label_element_above' => 'h3',
    'label_class_above' => '',
    'label_suffix_above' => '',
    'label_element_inline' => 'span',
    'label_class_inline' => '',
    'label_suffix_inline' => ':',
    'multiple_items_element' => '',
    'multiple_items_class' => '',
    'multiple_item_element' => 'div',
    'multiple_item_class' => 'pull-right',
    'multiple_item_separator' => '',
    'use_singlevalue_settings' => 1,
    'single_item_element' => '',
    'single_item_class' => '',
    'last_every_nth' => '0',
    'first_class' => 'first',
    'last_class' => 'last',
    'striping_classes' => 'odd even',
  );
  $export['pull_right'] = $preset;

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'well';
  $preset->admin_title = 'Well';
  $preset->description = '';
  $preset->data = array(
    'field_element' => '',
    'field_class' => '',
    'field_prefix' => '',
    'field_suffix' => '',
    'label_element_above' => 'h3',
    'label_class_above' => '',
    'label_suffix_above' => '',
    'label_element_inline' => 'span',
    'label_class_inline' => '',
    'label_suffix_inline' => ':',
    'multiple_items_element' => '',
    'multiple_items_class' => '',
    'multiple_item_element' => 'div',
    'multiple_item_class' => 'well',
    'multiple_item_separator' => '',
    'use_singlevalue_settings' => 1,
    'single_item_element' => '',
    'single_item_class' => '',
    'last_every_nth' => '0',
    'first_class' => 'first',
    'last_class' => 'last',
    'striping_classes' => 'odd even',
  );
  $export['well'] = $preset;

  return $export;
}
