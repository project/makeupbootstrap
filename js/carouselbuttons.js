/**
 * @file
 * CAROUSEL BOOTSTRAP BOUTONS DE CONTROLE 2.
 */

jQuery(function () {

  var $playButton = jQuery('#playButton'),
      $pauseButton = jQuery('#pauseButton');

  $playButton.click(function () {
    jQuery(Drupal.settings.makeupbootstrap_carousel_id).carousel('cycle');
    $pauseButton.show();
    $playButton.hide();
  });

  $pauseButton.click(function () {
    jQuery(Drupal.settings.makeupbootstrap_carousel_id).carousel('pause');
    $pauseButton.hide();
    $playButton.show();
  });
});
